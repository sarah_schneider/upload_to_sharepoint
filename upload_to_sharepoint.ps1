# Get filename from param
Param($FMFile)

# Specify credentials file and user account
$credsfile = 'creds.txt'
$User = 'pimshrpntusr@wgbh365.onmicrosoft.com'
$Password = Get-Content $credsfile | ConvertTo-SecureString

#$Password = Read-Host -Prompt "Please enter your password" -AsSecureString

# Specify site URL
$SiteURL = "https://wgbh365.sharepoint.com/sites/CLM_PIM/"
$DocLibName = "Clause Library"

# Add references to SharePoint client assemblies and authenticate to Office 365 site – required for CSOM
Add-Type -Path "C:\Program Files\Common Files\Microsoft Shared\Web Server Extensions\15\ISAPI\Microsoft.SharePoint.Client.dll"
Add-Type -Path "C:\Program Files\Common Files\Microsoft Shared\Web Server Extensions\15\ISAPI\Microsoft.SharePoint.Client.Runtime.dll"
Add-Type -Path "c:\Program Files\Common Files\microsoft shared\Web Server Extensions\15\ISAPI\Microsoft.SharePoint.Client.Taxonomy.dll"

# Bind to site collection
$Context = New-Object Microsoft.SharePoint.Client.ClientContext($SiteURL)
$Creds = New-Object Microsoft.SharePoint.Client.SharePointOnlineCredentials($User,$Password)
$Context.Credentials = $Creds
$Web = $Context.Web
$Context.Load($Web)
$Context.ExecuteQuery()

# Get Document Library list
$List = $Web.Lists.GetByTitle($DocLibName)
$Context.Load($List)
$Context.ExecuteQuery()

# Get EEC folder
$folder = $Web.getfolderbyserverrelativeurl($DocLibName)
$Context.Load($folder)
$Context.ExecuteQuery()

# Get list of files
$files = $folder.Files
$Context.Load($files)
$Context.ExecuteQuery()

# Upload file to SharePoint
$FMFilename = Split-Path -Path $FMFile -Leaf
$FMFilepath = Get-ItemProperty -Path $FMFile

$FileStream = New-Object IO.FileStream($FMFilepath,[System.IO.FileMode]::Open)
$FileCreationInfo = New-Object Microsoft.SharePoint.Client.FileCreationInformation
$FileCreationInfo.Overwrite = $true
$FileCreationInfo.ContentStream = $FileStream
$FileCreationInfo.URL = $FMFilename
$Upload = $List.RootFolder.Files.Add($FileCreationInfo)
$Context.Load($Upload)
$Context.ExecuteQuery()

# Get back Document ID
foreach ($item in $files)
{
  if ($item.Name -eq $FMFilename)
    {
      $Meta = $item.ListItemAllFields
      $Context.Load($Meta)
      $Context.ExecuteQuery();
      write-host "Sharepoint GUID: ", $Meta.fieldValues.GUID
    }
}