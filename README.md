# README #

This is a simple PowerShell script to be run from FileMaker. It takes a filename as a parameter and uploads the file into the WGBH SharePoint library for use with EEC. After the upload, it receives back a Document GUID.

To run:

```
powershell -command .\upload_to_sharepoint.ps1 '<filename>.docx'
```